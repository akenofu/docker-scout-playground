# docker-scout-playground
A dummy GitLab CI/CD pipeline that builds a simple Dockerfile for the DAST tool sslyze. The newly built image is pushed into GitLab container Registry, and scanned using the official free docker scout (A docker image scanning by docker.com). 

Docker Scout is available at: https://github.com/docker/scout-cli.

Sample Run:
![SAMPLE_RUN](https://gitlab.com/akenofu/docker-scout-playground/-/raw/main/Sample_Run.png)

Check the pipeline from: https://gitlab.com/akenofu/docker-scout-playground/-/jobs/6543389833


Download the job artifact from:
https://gitlab.com/akenofu/docker-scout-playground/-/jobs/6543389833/artifacts/download
